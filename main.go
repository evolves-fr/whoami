package main

import (
	"bytes"
	"embed"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/Masterminds/sprig"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

//go:embed index.gohtml
var indexTemplate string

//go:embed public/*
var public embed.FS

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.TraceLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	var r = echo.New()

	r.Use(middleware.Recover())
	r.Use(middleware.Logger())

	r.GET("/favicon.ico", static)
	r.GET("/favicon-16x16.png", static)
	r.GET("/favicon-32x32.png", static)
	r.GET("/apple-touch-icon.png", static)

	r.Any("/", info)
	r.Any("/*", info)

	if err := r.Start(":3000"); err != nil {
		panic(err)
	}
}

type RequestInformation struct {
	Hostname   string      `json:"hostname"`
	URL        *url.URL    `json:"url"`
	Host       string      `json:"host"`
	RequestURI string      `json:"requestURI"`
	Method     string      `json:"methode"`
	Protocol   string      `json:"protocol"`
	RemoteAddr string      `json:"remoteAddr"`
	RealIP     string      `json:"realIP"`
	Headers    http.Header `json:"headers"`
	Form       url.Values  `json:"form"`
	Body       interface{} `json:"body"`
}

func static(c echo.Context) error {
	file, err := public.Open(filepath.Join("public", c.Request().RequestURI))
	if err != nil {
		return err
	}
	defer file.Close()

	if data, err := ioutil.ReadAll(file); err == nil {
		c.Response().Header().Set("Cache-Control", "max-age=31536000")
		return c.Blob(http.StatusOK, mime.TypeByExtension(filepath.Ext(c.Request().RequestURI)), data)
	}

	return echo.NewHTTPError(http.StatusNotFound, http.StatusText(http.StatusNotFound))
}

func info(c echo.Context) error {
	var err error

	// Parse form
	if strings.Contains(c.Request().Header.Get(echo.HeaderContentType), "multipart/form-data") {
		err = c.Request().ParseMultipartForm(1024 * 1024 * 20)
	} else if strings.Contains(c.Request().Header.Get(echo.HeaderContentType), "application/x-www-form-urlencoded") {
		err = c.Request().ParseForm()
	}
	if err != nil {
		return err
	}

	var body interface{}
	// Parse body
	if c.Request().Body != nil {
		bodyBytes, err := ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return err
		}

		if strings.Contains(c.Request().Header.Get(echo.HeaderContentType), echo.MIMEApplicationJSON) {
			if err = json.Unmarshal(bodyBytes, &body); err != nil {
				return err
			}
		} else {
			body = string(bodyBytes)
		}
	}

	// Get hostname
	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	// Set request URL host
	if c.Request().Host != "" {
		c.Request().URL.Host = c.Request().Host
	}

	// Set request URL scheme
	c.Request().URL.Scheme = "http"
	if c.Request().TLS != nil {
		c.Request().URL.Scheme = "http"
	}

	// Set data
	var data = &RequestInformation{
		Hostname:   hostname,
		Host:       c.Request().Host,
		URL:        c.Request().URL,
		RequestURI: c.Request().RequestURI,
		Method:     c.Request().Method,
		Protocol:   c.Request().Proto,
		RemoteAddr: c.Request().RemoteAddr,
		RealIP:     c.RealIP(),
		Headers:    c.Request().Header,
		Form:       c.Request().Form,
		Body:       body,
	}

	// Return data format for accept header
	var acceptHeader = c.Request().Header.Get(echo.HeaderAccept)
	switch {
	case hasAccept(acceptHeader, echo.MIMEApplicationJSON):
		return c.JSON(http.StatusOK, data)
	case hasAccept(acceptHeader, echo.MIMETextHTML, "*/*"):
		var (
			tmpl   = template.Must(template.New("index.html").Funcs(sprig.FuncMap()).Parse(indexTemplate))
			buffer = bytes.Buffer{}
		)
		if err := tmpl.Execute(&buffer, data); err != nil {
			return err
		}
		return c.HTMLBlob(http.StatusOK, buffer.Bytes())
	default:
		return echo.NewHTTPError(http.StatusNotAcceptable, c.Request().Header.Get(echo.HeaderAccept)+"not acceptable")
	}
}

func hasAccept(actual string, exceptes ...string) bool {
	var accepts = make([]string, 0)

	for _, item := range strings.Split(strings.TrimSpace(actual), ",") {
		if values := regexp.MustCompile(`(?:;.*)?([a-zA-Z*]+/[a-zA-Z*]+)(?:;.*)?`).FindStringSubmatch(item); len(values) == 2 {
			accepts = append(accepts, values[1])
		}
	}

	for _, accept := range accepts {
		for _, excepte := range exceptes {
			if accept == excepte {
				return true
			}
		}
	}

	return false
}
