.PHONY: install
install:
	wget -qO- https://github.com/traefik/traefik/releases/download/v2.6.3/traefik_v2.6.3_darwin_amd64.tar.gz | tar xvz - -C .dev traefik

.PHONY: proxy
proxy:
	.dev/traefik --configFile=.dev/traefik.yaml

.PHONY: build
build:
	goreleaser release --snapshot --rm-dist

.PHONY: release
release:
	goreleaser release --rm-dist
